import tkinter as tk
from tkinter import Label, Button, IntVar, E, W, N, S, Radiobutton, Toplevel
from tkinter.constants import DISABLED, NORMAL
from tkinter.ttk import Scale
import tkinter.messagebox as tkMessageBox
from PIL import Image, ImageTk
import socket, threading, os, time
import math

from RtpPacket import RtpPacket

CACHE_FILE_NAME = "cache-"
CACHE_FILE_EXT = ".jpg"

class Client:
	INIT = 0
	READY = 1
	PLAYING = 2
	state = INIT
	
	SETUP = 0
	PLAY = 1
	PAUSE = 2
	TEARDOWN = 3
	# Extension 3
	DESCRIBE = 4 
	trigger = False
	SEEK = 5

	SWITCH = 6
	
	# Initiation..
	def __init__(self, master, serveraddr, serverport, rtpport, filename):
		self.master = master
		self.master.protocol("WM_DELETE_WINDOW", self.handler)
		self.createWidgets()
		self.serverAddr = serveraddr
		self.serverPort = int(serverport)
		self.rtpPort = int(rtpport)
		self.fileName = filename
		self.rtspSeq = 0
		self.sessionId = 0
		self.requestSent = -1
		self.teardownAcked = 0
		self.connectToServer()
		self.frameNbr = 0
		# Extension 1: Variables for loss rate and data rate
		self.totalBytes = 0
		self.startTime = 0
		self.totalPlayTime = 0
		self.packetLost = 0
		self.lossRate = 0
		self.dataRate = 0
		self.totalFrame = 0


		# Extension 4
		self.fps = 25 # unit: frame/sec
		self.totalTime = 0 # unit: sec
		self.seekFlag = False

		# Extension 5
		self.enableBtnByDefault()
		
	def createWidgets(self):
		"""Build GUI."""
		# Extension 2: No SETUP button
		# Create Setup button
		# self.setup = Button(self.master, width=20, padx=3, pady=3, background="#f0d1f5")
		# self.setup["text"] = "Setup"
		# self.setup["command"] = self.setupMovie
		# self.setup.grid(row=1, column=0, padx=2, pady=2)
		
		# Create Play button		
		self.start = Button(self.master, width=20, padx=3, pady=3, background="#f5f0d1")
		self.start["text"] = "Play"
		self.start["command"] = self.playMovie
		self.start.grid(row=2, column=0, padx=2, pady=2)
		
		# Create Pause button			
		self.pause = Button(self.master, width=20, padx=3, pady=3, background="#d1f5f0")
		self.pause["text"] = "Pause"
		self.pause["command"] = self.pauseMovie
		self.pause.grid(row=2, column=1, padx=2, pady=2)

		# Extension 5: Create Switch button
		self.switch = Button(self.master, width=20, padx=3, pady=3, background="#e87e3c")
		self.switch["text"] = "Switch"
		self.switch["command"] = self.switchMovie
		self.switch.grid(row=0, column=0, padx=2, pady=2, sticky=E+W)

		# Extension 3: Create Describe button
		self.describe = Button(self.master, width=20, padx=3, pady=3, background="#d6f5d1")
		self.describe["text"] = "Describe"
		self.describe["command"] = self.describeStream
		self.describe.grid(row=2, column=2, padx=2, pady=2)

		# Create Teardown button
		self.teardown = Button(self.master, width=20, padx=3, pady=3, background="#f5d1d6")
		self.teardown["text"] = "Stop"
		self.teardown["command"] =  self.exitClient
		self.teardown.grid(row=2, column=3, padx=2, pady=2)

		# Create a label to display the movie
		self.label = Label(self.master, height=19)
		self.label.grid(row=1, column=0, columnspan=4, sticky=W+E+N+S, padx=5, pady=5) 

		# Extension 1: Create a label to display movie stat
		# Packet loss rate
		self.label2 = Label(self.master)
		self.label2["text"] = "RTP packet loss rate: 0"
		self.label2.grid(row=4, column=0, columnspan=4, padx=5, pady=5)
		# Video data rate
		self.label3 = Label(self.master)
		self.label3["text"] = "Video data rate (bytes/sec): 0"
		self.label3.grid(row=5, column=0, columnspan=4, padx=5, pady=5)

		# Extension 4: Progress bar
		self.slider_val = IntVar()
		self.slider = Scale(self.master, from_=0, to=100, orient='horizontal', variable=self.slider_val, command=self.userSeek)
		self.slider_val.set(0)
		self.slider.grid(row=6, column=1, columnspan=2, padx=5, pady=5, sticky=E+W)

		# Timer display
		# The left one
		self.timer_up = Label(self.master)
		self.timer_up["text"] = "00:00"
		self.timer_up.grid(row=6, column=0, sticky=E+W)

		# The right one
		self.total_time = Label(self.master)
		self.total_time["text"] = "00:00"
		self.total_time.grid(row=6, column=3, sticky=E+W)

		# Confirm seeking button
		self.confirmSeek = Button(self.master, padx=3, pady=3, command=self.handleSeekEvent, background="#f0d1f5")
		self.confirmSeek["text"] = "Seek"
		self.confirmSeek["state"] = DISABLED
		self.confirmSeek.grid(row=7, column=0, columnspan=2, padx=2, pady=2, sticky=E+W)

		self.seekStatus = Label(self.master)
		self.seekStatus["text"] = ""
		self.seekStatus.grid(row=7, column=2, columnspan=2, padx=5, pady=5, sticky=E+W)

		# 3 buttons for 3 videos
		self.video_1 = Button(self.master, width=20, padx=3, pady=3, background="#d4e3fa")
		self.video_1["text"] = "movie.Mjpeg"
		self.video_1["state"] = DISABLED
		self.video_1["command"] = lambda: self.getMovie("movie.Mjpeg")
		self.video_1.grid(row=0, column=1, padx=2, pady=2)
		
		self.video_2 = Button(self.master, width=20, padx=3, pady=3, background="#d4e3fa")
		self.video_2["text"] = "movie_2.Mjpeg"
		self.video_2["state"] = DISABLED
		self.video_2["command"] = lambda: self.getMovie("movie_2.Mjpeg")
		self.video_2.grid(row=0, column=2, padx=2, pady=2)

		self.video_3 = Button(self.master, width=20, padx=3, pady=3, background="#d4e3fa")
		self.video_3["text"] = "movie_3.Mjpeg"
		self.video_3["state"] = DISABLED
		self.video_3["command"] = lambda: self.getMovie("movie_3.Mjpeg")
		self.video_3.grid(row=0, column=3, padx=2, pady=2)

	# Extension 1
	def updateStat(self):
		self.label2["text"] = "RTP packet loss rate: " + str(self.lossRate)
		self.label3["text"] = "Video data frame (bytes/sec): " + str(self.dataRate)

	# Extension 4
	def updateTimerUpUI(self):
		portion = self.frameNbr / self.totalFrame
		time_elapsed = int(portion * self.totalTime)
		total_time_minutes = time_elapsed // 60
		total_time_secs = time_elapsed % 60
		self.timer_up["text"] = f'{total_time_minutes:02d}:{total_time_secs:02d}'

	def updateTotalTimeUI(self):
		total_time_minutes = self.totalTime // 60
		total_time_secs = self.totalTime % 60
		self.total_time["text"] = f'{total_time_minutes:02d}:{total_time_secs:02d}'

	def userSeek(self, newValue):
		if self.state == self.READY:
			self.seekFlag = True
			self.newFrameNum = int(float(newValue) * self.totalFrame / 100)

	def setupMovie(self):
		"""Setup button handler."""
		if self.state == self.INIT:
			self.sendRtspRequest(self.SETUP)
	
	def exitClient(self):
		"""Teardown button handler."""
		self.sendRtspRequest(self.TEARDOWN)
		# Close the GUI
		self.master.destroy()
		try:
			os.remove(CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT) # Delete the cache image from video
		except:
			"No cache file to delete."
			None

	def pauseMovie(self):
		"""Pause button handler."""
		if self.state == self.PLAYING:
			self.sendRtspRequest(self.PAUSE)
	
	def playMovie(self):
		"""Play button handler."""
		# Extension 2: Press PLAY button will automatically setup movie
		self.setupMovie()

		# Return "Seek" button to normal state
		self.confirmSeek["state"] = NORMAL

		if self.state == self.READY:
			# Condition to play video for extension 5
			if not (self.video_1["state"] == NORMAL and self.video_2["state"] == NORMAL and self.video_3["state"] == NORMAL):
				self.startTime = time.time()
				self.trigger = True # Extension 3
				threading.Thread(target=self.listenRtp).start()
				self.playEvent = threading.Event()
				self.playEvent.clear()
				self.seekStatus['text'] = ''
				self.sendRtspRequest(self.PLAY)

	# Extension 5: Default movie button handler
	def enableBtnByDefault(self):
		if self.fileName == "movie.Mjpeg":
			self.video_1["state"] = NORMAL
			self.video_1["background"] = "#eb534d"
		elif self.fileName == "movie_2.Mjpeg":
			self.video_2["state"] = NORMAL
			self.video_2["background"] = "#eb534d"
		elif self.fileName == "movie_3.Mjpeg":
			self.video_3["state"] = NORMAL
			self.video_3["background"] = "#eb534d"
		else:
			None

	# Extension 5: Switch button handler
	def switchMovie(self):
		"""Switch button hanlder."""
		# From extension 5:
		if self.state is not self.PLAYING:
			self.video_1["state"] = NORMAL
			self.video_1["background"] = "#d4e3fa"

			self.video_2["state"] = NORMAL
			self.video_2["background"] = "#d4e3fa"

			self.video_3["state"] = NORMAL
			self.video_3["background"] = "#d4e3fa"

			# Prevent "Seek" while switching video
			self.confirmSeek["state"] = DISABLED

	def getMovie(self, movieName):
		# Disable every other movie buttons, but leave the chosen one
		self.video_1["state"] = DISABLED
		self.video_2["state"] = DISABLED
		self.video_3["state"] = DISABLED

		if movieName == "movie.Mjpeg":
			self.video_1["state"] = NORMAL
			self.video_1["background"] = "#eb534d" # This is red color

		elif movieName == "movie_2.Mjpeg":
			self.video_2["state"] = NORMAL
			self.video_2["background"] = "#eb534d" # This is red color

		else:
			self.video_3["state"] = NORMAL
			self.video_3["background"] = "#eb534d" # This is red color
		
		# If the video is not in pause mode, clicking on the movie name will not send rtsp request
		# If the new video is not different to the old video, nothing will be sent too	
		if self.state != self.PLAYING and self.fileName != movieName:
			# Assign to fileName
			self.fileName = movieName

			# Reset timer up to 00:00
			self.timer_up["text"] = "00:00"

			self.sendRtspRequest(self.SWITCH)
		
	# Extension 3: Describe button
	def describeStream(self):
		"""Describe button handler."""
		if self.trigger:
			self.sendRtspRequest(self.DESCRIBE)

	# Extension 4: Handle Seek Event
	def handleSeekEvent(self):
		if (self.seekFlag):
			self.seekFlag = False
			self.seekStatus['text'] = 'Status: Waiting'
			self.sendRtspRequest(self.SEEK)
		else:
			# normal, just continue
			return

	def listenRtp(self):		
		"""Listen for RTP packets."""
		while True:
			try:
				data = self.rtpSocket.recv(20480)

				# Extension 1: Update total time & start time
				currTime = time.time()
				self.totalPlayTime += currTime - self.startTime
				self.startTime = currTime

				if data:
					rtpPacket = RtpPacket()
					rtpPacket.decode(data)

					# Extension 1: Update the total data bytes
					self.totalBytes += len(rtpPacket.getPayload())

					currFrameNum = rtpPacket.seqNum()
					# print(f'Current Seq Number: {currFrameNum}')

					# Extension 1
					if (self.frameNbr + 1) != currFrameNum:
						self.packetLost += math.abs(currFrameNum - (self.frameNbr + 1))
						print('!'*50 + "\nPACKET LOSS\n" + '!'*50)
					# Update the loss rate
					if self.packetLost == 0:
						self.lossRate = 0
					else:
						self.lossRate = self.packetLost / currFrameNum
					# Update data rate
					self.dataRate = round(self.totalBytes / self.totalPlayTime, 5)
					# Update the stat of the movie
					self.updateStat()

					if currFrameNum > self.frameNbr:
						self.frameNbr = currFrameNum
						# Extension 4
						self.slider_val.set(int(self.frameNbr * 100 / self.totalFrame))
						self.updateTimerUpUI()
						self.updateMovie(self.writeFrame(rtpPacket.getPayload()))
			except:
				# Stop listening upon requesting PAUSE or TEARDOWN
				if self.playEvent.isSet():
					break
				
				# Upon receiving ACK for TEARDOWN request, close RTP socket
				if self.teardownAcked == 1:
					self.rtpSocket.shutdown(socket.SHUT_RDWR)
					self.rtpSocket.close()
					break
					
	def writeFrame(self, data):
		"""Write the received frame to a temp image file. Return the image file."""
		cachename = CACHE_FILE_NAME + str(self.sessionId) + CACHE_FILE_EXT
		file = open(cachename, 'wb')
		file.write(data)
		file.close()

		return cachename
	
	def updateMovie(self, imageFile):
		"""Update the image file as video frame in the GUI."""
		photo = ImageTk.PhotoImage(Image.open(imageFile))
		self.label.configure(image=photo, height=288)
		self.label.image = photo
		
	def connectToServer(self):
		"""Connect to the Server. Start a new RTSP/TCP session."""
		self.rtspSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.rtspSocket.connect((self.serverAddr, self.serverPort))
		except:
			tkMessageBox.showwarning('Connection Failed', f'Connection to \'{self.serverAddr}\' failed')
	
	def sendRtspRequest(self, requestCode):
		"""Send RTSP request to the server."""	
		# setup request
		if requestCode == self.SETUP and self.state == self.INIT:
			threading.Thread(target=self.recvRtspReply).start()
			# update RTSP sequence number
			self.rtspSeq += 1

			# Write the RTSP request to be sent
			request = f'SETUP {self.fileName} RTSP/1.0\nCSeq: {self.rtspSeq}\nTransport: RTP/UDP; client_port= {self.rtpPort}'

			self.requestSent = self.SETUP

		# play request
		elif requestCode == self.PLAY and self.state == self.READY:
			# update RTSP sequence number
			self.rtspSeq += 1

			# Write the RTSP request to be sent
			request = f'PLAY {self.fileName} RTSP/1.0\nCSeq: {self.rtspSeq}\nSession: {self.sessionId}'

			self.requestSent = self.PLAY

		# pause request
		elif requestCode == self.PAUSE and self.state == self.PLAYING:
			# update RTSP sequence number
			self.rtspSeq += 1

			# Write the RTSP request to be sent
			request = f'PAUSE {self.fileName} RTSP/1.0\nCSeq: {self.rtspSeq}\nSession: {self.sessionId}'

			self.requestSent = self.PAUSE

		# Extension 3: describe request
		elif requestCode == self.DESCRIBE:
			self.rtspSeq += 1

			request = f'DESCRIBE {self.fileName} RTSP/1.0\nCSeq: {self.rtspSeq}\nSession: {self.sessionId}'
			
			self.requestSent = self.DESCRIBE

		# Extension 4: seek frame
		elif requestCode == self.SEEK and self.state == self.READY:
			self.rtspSeq += 1
			request = f'SEEK {self.fileName} {self.newFrameNum} RTSP/1.0\nCSeq: {self.rtspSeq}\nSession: {self.sessionId}'

			self.requestSent = self.SEEK

		# teardown request
		elif requestCode == self.TEARDOWN and self.state != self.INIT:
			# update RTSP sequence number
			self.rtspSeq += 1

			# Write the RTSP request to be sent
			request = f'TEARDOWN {self.fileName} RTSP/1.0\nCSeq: {self.rtspSeq}\nSession: {self.sessionId}'

			self.requestSent = self.TEARDOWN

		# Extension 5: switch request
		elif requestCode == self.SWITCH and self.state != self.INIT:
			# update RTSP sequence number
			self.rtspSeq += 1

			# Write the RTSP request to be sent
			request = f'SWITCH {self.fileName} RTSP/1.0\nCSeq: {self.rtspSeq}\nSession: {self.sessionId}'

			self.requestSent = self.SWITCH

		else:
			return

		# send the RTSP request using rtspSocket
		self.rtspSocket.send(bytes(request, 'utf-8'))
	
		print('\nData sent:\n' + request)
	
	def recvRtspReply(self):
		"""Receive RTSP reply from the server."""
		while True:
			reply = self.rtspSocket.recv(1024)

			if reply:
				print(f'Data received:\n{reply.decode("utf-8")}\n')
				self.parseRtspReply(reply.decode('utf-8'))

			# Close the RTSP socket upon requesting TEARDOWN
			if self.requestSent == self.TEARDOWN:
				self.rtspSocket.shutdown(socket.SHUT_RDWR)
				self.rtspSocket.close()
				break
	
	def parseRtspReply(self, data):
		"""Parse the RTSP reply from the server."""
		lines = data.split('\n')
		seqNum = int(lines[1].split(' ')[1])

		# only process if seq number is match with request seqNum
		if seqNum == self.rtspSeq:
			session = int(lines[2].split(' ')[1])
			# New RTSP session ID
			if self.sessionId == 0:
				self.sessionId = session

			# Process only if session ID is the same
			if self.sessionId == session:
				status = int(lines[0].split(' ')[1])
				if status == 200:
					# If request sent is SETUP
					if self.requestSent == self.SETUP:
						# Update RTSP state
						self.state = self.READY

						# Open RTP port
						self.openRtpPort()
						self.playMovie() # Extension 2
						
						# Extension 4
						# get total frame here

						self.totalFrame = int(lines[-1].split(' ')[1])
						self.totalTime = self.totalFrame // self.fps
						self.updateTotalTimeUI()

					# If request sent is PLAY
					elif self.requestSent == self.PLAY:
						self.state = self.PLAYING

					# If request sent is PAUSE
					elif self.requestSent == self.PAUSE:
						self.state = self.READY

						# The play thread exits. New thread is created to resume
						self.playEvent.set()
					
					# If request sent is SEEK
					elif self.requestSent == self.SEEK:
						self.state = self.READY
						self.seekStatus['text'] = 'Status: OK'
						self.frameNbr = self.newFrameNum

					# If request sent is TEARDOWN
					elif self.requestSent == self.TEARDOWN:
						self.state = self.INIT

						# Flag the ACK for teardown to close the socket
						self.teardownAcked = 1
					
					# If request sent is SWITCH
					elif self.requestSent == self.SWITCH:
						self.openRtpPort()

						# Reset the frame number and update the timer
						self.frameNbr = 0
						self.totalFrame = int(lines[-1].split(' ')[1])
						self.totalTime = self.totalFrame // self.fps
						self.updateTotalTimeUI()

						# Reset state
						self.state = self.READY						

	def openRtpPort(self):
		"""Open RTP socket binded to a specified port."""
		# Create a new datagram socket to receive RTP packets from the server
		self.rtpSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

		# Set the timeout value of the socket to 0.5sec
		self.rtpSocket.settimeout(0.5)

		try:
			# Bind the socket to the address using the RTP port given by the client user
			self.rtpSocket.bind(("", self.rtpPort))
		except:
			tkMessageBox.showwarning('Unable to Bind', f'Unable to bind PORT={self.rtpPort}')

	def handler(self):
		"""Handler on explicitly closing the GUI window."""
		self.pauseMovie()
		quitRequest = tkMessageBox.askokcancel(title='Exit', message='Are you sure you want to exit?')
		if quitRequest:
			tkMessageBox.showinfo(title='Status', message='You have exited successfully!')
			self.exitClient()
		else:
			# When user presses cancel, continue playing
			self.playMovie()