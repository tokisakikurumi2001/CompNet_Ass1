class VideoStream:
	def __init__(self, filename):
		self.filename = filename
		try:
			self.file = open(filename, 'rb')
		except:
			raise IOError
		self.frameNum = 0

		# first attempt to read total number of frame
		# We open the file in another variable
		# We try to read the file, firstly 5 bytes to get the length (number of bytes) of the frame
		# After that, using this number to obtain all frame
		# Continue reading 5 bytes -> obtain frame.
		# This process stop when we can read furthermore. EOF
		cache_file = open(filename, 'rb')
		data = cache_file.read(5)
		self.totalFrame = 0
		while data:
			# Still can read
			framelen = int(data)
			data = cache_file.read(framelen)
			self.totalFrame += 1
			data = cache_file.read(5)
		cache_file.close()

	def seek(self, frameNum):
		self.file.seek(0)
		self.frameNum = frameNum - 1
		cnt = 0
		while cnt < self.frameNum:
			data = self.file.read(5)
			framelen = int(data)
			_ = self.file.read(framelen)
			cnt += 1
		return True

	def totalNumFrame(self):
		return self.totalFrame
		
	def nextFrame(self):
		"""Get next frame."""
		data = self.file.read(5) # Get the framelength from the first 5 bits
		if data: 
			framelength = int(data)
							
			# Read the current frame
			data = self.file.read(framelength)
			self.frameNum += 1
		return data
		
	def frameNbr(self):
		"""Get frame number."""
		return self.frameNum
	
	