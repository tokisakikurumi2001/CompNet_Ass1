# Assignment 1 of Computer Network: Video Streaming Application

![extend5_GUI](assets/extend5_GUI.png)

## Features

- Select different range of videos available.
- Play, pause to interact with the video
- Slider for user to select a specific time of the video

## SETUP

To run this application, it is assumed that you have the python environment installed on your machine.

**NOTE**: You can utilize the python virtual environment if needed.

### Install required package

```bash
pip install -r requirements.txt
```

### Run the server

```bash
cd src/
python Server.py 5555
```

### Run the client

Open another terminal.

```bash
cd src/
python ClientLauncher.py localhost 5555 101 Movie.Mjpeg
```
